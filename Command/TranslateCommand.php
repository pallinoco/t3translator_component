<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package TYPO3
 * @subpackage Translation
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */
namespace Pallino\Translator\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Pallino\Translator\Utility\IO;
use Pallino\Translator\Utility\XliffUtility;

/**
 * Class TranslateCommand command to execute the extracting from template file of all translation
 *
 * @package Pallino\Translator\Command
 */
class TranslateCommand extends Command {
    protected function configure() {
        $this
            ->setName('translate')
            ->setDescription('Utility for extract translation keys from extension file, and save them to excel or xliff file.')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Path to TYPO3 Installation'
            )
            ->addArgument(
                'extension',
                InputArgument::REQUIRED,
                'Extension name (* for all)'
            )
            ->addArgument(
                'language',
                InputArgument::REQUIRED,
                'Default language (2 character ISO code)'
            )
            ->addArgument(
                'destinationPath',
                InputArgument::REQUIRED,
                'Destination path (the file will be named translated.xls and default.xlf'
            )
            ->addOption(
                'exportFormat',
                null,
                InputOption::VALUE_OPTIONAL,
                'xls or xliff format',
                'xls'
            );
    }

    /**
     * Executes the translation operation
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output) {
        //get Arguments and options
        $path = $input->getArgument('path');
        $extension = $input->getArgument('extension');
        $destinationPath = $input->getArgument('destinationPath');
        $language = $input->getArgument('language');
        //check if option is set to valid values
        $exportFormat = $input->getOption('exportFormat');
        if ($exportFormat != 'xls' && $exportFormat != 'xliff' && $exportFormat != 'all') {
            throw new \Exception('export format must be xls or xliff.');
        }
        if ($extension == '*') {
            $extension = '';
        } else {
            $extension = DIRECTORY_SEPARATOR . $extension;
        }
        //set extension path
        $extensionPath = $path . DIRECTORY_SEPARATOR . 'typo3conf' . DIRECTORY_SEPARATOR . 'ext' . $extension;
        if (!realpath($extensionPath)) {
            throw new \Exception('Source path for extension: ' . $extensionPath . ' not found.');
        }
        //get translation from TYPO3 files
        $translatingLabels = $this->getTranslations($extensionPath, $language);
        //adds the default language
        $elaboratedArray = array('languages' => array($language)) + $translatingLabels;
        if (!file_exists($destinationPath)) {
            throw new \Exception('destination path ' . $extensionPath . ' not found.');
        }
        //define files name and path
        $xliffDestinationFile = $destinationPath . DIRECTORY_SEPARATOR . 'default.xlf';
        $xlsDestinationFile = $destinationPath . DIRECTORY_SEPARATOR . 'translate.xls';
        //execute export from format
        switch ($exportFormat) {
            case 'xls':
                //export to excel file
                IO::saveTranslationToExcel($elaboratedArray, $xlsDestinationFile);
                break;
            case 'xliff':
                //export to xliff file
                XliffUtility::writeToFileXliff($xliffDestinationFile,$elaboratedArray, $language, $language);
                break;
            case 'all':
                //export both excel file and xliff file
                XliffUtility::writeToFileXliff($xliffDestinationFile,$elaboratedArray, $language, $language);
                IO::saveTranslationToExcel($elaboratedArray, $xlsDestinationFile);
                break;
        }
    }

    /**
     * Searches for all extensions html files and extract the translation key and default text
     *
     * @param string $path path to base dir for extracting files
     * @param string $language the language to use
     * @return array
     */
    protected function getTranslations($path, $language = 'en') {
        $finder = new Finder();
        $files = $finder->files()->name('*.html')->in($path);
        $translatingLabels = array();
        foreach ($files as $file) {
            /** @var \Symfony\Component\Finder\SplFileInfo $file */
            $translatingLabels = $this->getTranslationLabelFromTemplateFile($file->getPathname(), $language, $translatingLabels);

        }
        return $translatingLabels;
    }

    /**
     * Gets translation data from template file
     *
     * @param string $inFile path to extract translation
     * @param string $language default language
     * @param array $array translation array to update
     *
     * @return array translation array updated
     */
    protected function getTranslationLabelFromTemplateFile($inFile, $language = 'en', $array = array()) {
        $text = file_get_contents($inFile);
        preg_match_all('/locallang\.xlf\:(.*)\"[\s]*\>[\s]*(.*)[\s]*<\/f\:translate>/iUS', $text, $matches);
        foreach ($matches[1] as $key => $match) {
            $array[$match] = array($language => trim($matches[2][$key]));
        }
        return $array;
    }
}