<?php
namespace Pallino\Translator\Command;

use Pallino\Translator\Utility\IO;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ExportCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('export')
            ->setDescription('import')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Path to TYPO3 Installation'
            )
            ->addArgument(
                'extension',
                InputArgument::REQUIRED,
                'Extension name'
            )
            ->addArgument(
                'destinationPath',
                InputArgument::REQUIRED,
                'Destination path'
            )
            ->addArgument(
                'defaultLanguage',
                InputArgument::REQUIRED,
                'Default Language'
            )
            ->addArgument(
                'languages',
                InputArgument::IS_ARRAY,
                'List of languages (2 character ISO code)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        //get Arguments and options
        $path = $input->getArgument('path');
        $extension = $input->getArgument('extension');
        $destinationPath = $input->getArgument('destinationPath');
        $languages = $input->getArgument('languages');
        $defaultLanguage = $input->getArgument('defaultLanguage');
        //set extension path
        $extensionPath = $path . DIRECTORY_SEPARATOR . 'typo3conf' . DIRECTORY_SEPARATOR . 'ext' . DIRECTORY_SEPARATOR . $extension . DIRECTORY_SEPARATOR . 'Resources' . DIRECTORY_SEPARATOR . 'Private' . DIRECTORY_SEPARATOR . 'Language';
        if (!realpath($extensionPath)) {
            throw new \Exception('Source path for extension: ' . $extensionPath . ' not found.');
        }
        $translations = array();
        foreach($languages as $language){
            $prepend = '';
            $tag = 'source';
            if($language != $defaultLanguage){
                $prepend = $language . '.';
                $tag = 'target';
            }
            $file = $extensionPath . DIRECTORY_SEPARATOR . $prepend . 'locallang.xlf';
            $translations = $this->getTranslationLabelFromTranslationFileWithTranslated($file, $translations,$language, $tag);
        }
        //get translation from TYPO3 files
       // $translatingLabels = $this->getTranslations($extensionPath, $language);
        //adds the default language
        $elaboratedArray = array('languages' => $languages) + $translations;
        if (!file_exists($destinationPath)) {
            throw new \Exception('destination path ' . $extensionPath . ' not found.');
        }
        //define files name and path
        $xlsDestinationFile = $destinationPath . DIRECTORY_SEPARATOR . 'translate.xls';
        IO::saveTranslationToExcel($elaboratedArray, $xlsDestinationFile);
    }

    public function getTranslationLabelFromTranslationFileWithTranslated($inFile, $array = array(),$language,$tag = 'source')
    {
        $text = file_get_contents($inFile);
        //$query = '<trans-unit[\s]*id="(.*)".*>.*[\s]*<'. $tag . '>(.*)<\/' . $tag . '>[\s]*';
        $query = 'id="(.*)".*(\\s*.*\\s*)<'. $tag . '>(.*)<\/' . $tag . '>*';
        //$query = '<trans-unit[\s]*id="(.*)".*>[\s]*<source>(.*)<\/source>[\s]*';
        preg_match_all('/' . $query . '/iUS', $text, $matches);
        // preg_match_all('/<trans-unit[\s]*id="(.*)".*>[\s]*<source>(.*)<\/source>[\s]*/iUS', $text, $matches);

        foreach ($matches[1] as $key => $match) {
            $array[$match][$language] = $matches[3][$key];
        }
        return $array;
    }
}