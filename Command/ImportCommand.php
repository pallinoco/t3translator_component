<?php
/***************************************************************
 *
 *  The MIT License (MIT)
 *
 *  Copyright (c) 2015 Federico Bernardin, http://www.pallino.it
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a copy
 *  of this software and associated documentation files (the "Software"), to deal
 *  in the Software without restriction, including without limitation the rights
 *  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *  copies of the Software, and to permit persons to whom the Software is
 *  furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *  THE SOFTWARE.
 *
 ***************************************************************/

/**
 * @package TYPO3
 * @subpackage Translation
 * @company Pallino & Co.
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created
 */
namespace Pallino\Translator\Command;

use Pallino\Translator\Utility\IO;
use Pallino\Translator\Utility\XliffUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ImportCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('import')
            ->setDescription('import')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Path to excel file'
            )
            ->addArgument(
                'destinationPath',
                InputArgument::REQUIRED,
                'Destination path'
            )
            ->addArgument(
                'defaultLanguage',
                InputArgument::REQUIRED,
                'Default Language'
            )
        ;
    }

    /**
     * Executes the exporting
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $path = $input->getArgument('path');
        $destinationPath = $input->getArgument('destinationPath');
        $defaultLanguage = $input->getArgument('defaultLanguage');
        $elaboratedArray = IO::getTranslationFromExcel($path);
        $languages = $elaboratedArray['languages'];
        foreach($languages as $language){
            if($language == $defaultLanguage){
                $languagePrepend = '';
            }
            else{
                $languagePrepend = $language . '.';
            }
            $xliffDestinationFile = $destinationPath . DIRECTORY_SEPARATOR . $languagePrepend . 'locallang.xlf';
            XliffUtility::writeToFileXliff($xliffDestinationFile,$elaboratedArray, $language);
        }
    }
}