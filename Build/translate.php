#!/usr/bin/env php
<?php
// application.php

require __DIR__.'/vendor/autoload.php';

use Pallino\Translator\Command\TranslateCommand;
use Symfony\Component\Console\Application;

$application = new Application();
$application->add(new TranslateCommand());
$application->add(new \Pallino\Translator\Command\ExportCommand());
$application->add(new \Pallino\Translator\Command\ImportCommand());
$application->run();