<?php
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 15/02/16
 */

namespace Pallino\Translator\Utility;
class IO {

    /**
     * Export translation data to file excel
     *
     * @param array $translatedData contains couple key value for translation
     * @param string $language two character ISO code for default language
     * @param string $file absolute path to destination file
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public static function saveTranslationToExcel($translatedData,$file) {
        $objPHPExcel = new \PHPExcel();
        $iteration = 1;
        $languages = array_shift($translatedData);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $iteration, 'KEY');
        $asciiValue = ord('A')+1;
        foreach($languages as $language) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue(chr($asciiValue) . $iteration, strtoupper($language));
            $asciiValue++;
        }
        $asciiValue = ord('A')+1;
        foreach($translatedData as $key => $value) {
            $iteration++;
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $iteration, $key);
            $asciiValue = ord('A')+1;
            foreach($languages as $language) {
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue(chr($asciiValue) . $iteration, $value[$language]);
                $asciiValue++;
            }
        }
        $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save($file);
    }

    public static function prepareTranslationDataForMultipleRow($translatedData) {
        //$row = array_rand($translatedData);
        $languages = array_shift($translatedData);
        if(!is_array($languages)){
            foreach($translatedData as $key => $value){
                $fixedArray[$key] = array( $languages => $value);
            }
            $translatedData = $fixedArray;
        }
        return $translatedData;
    }

    public static function getTranslationFromExcel($file) {
        //first column is key, second column is default

        $objReader = \PHPExcel_IOFactory::createReader('Excel2007');
        $objPHPExcel = $objReader->load($file);
        foreach($objPHPExcel->getActiveSheet()->getRowIterator() as $key => $value){
            if($key == 1){
                foreach($value->getCellIterator() as $subkey => $item){
                    if($item != 'KEY'){
                        $languages[ord($subkey)-ord('A')] = strtolower($objPHPExcel->getActiveSheet()->getCell($subkey . $key)->getValue());
                    }
                }
            }
            else{
                foreach($value->getCellIterator() as $subkey => $item){
                    if($subkey != 'A'){
                        $translation[$translationKey][$languages[ord($subkey)-ord('A')]] = $objPHPExcel->getActiveSheet()->getCell($subkey . $key)->getValue();
                    }
                    else{
                        $translationKey = $objPHPExcel->getActiveSheet()->getCell($subkey . $key)->getValue();
                    }
                }
            }
        }
        $elaboratedArray = array('languages' => $languages) + $translation;
        return $elaboratedArray;
    }
}