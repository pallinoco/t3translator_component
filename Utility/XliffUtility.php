<?php
/**
 * This file is part of a Pallino Project.
 *
 * It is a reserved code the Pallino & Co. has the intellectual property
 * of the code.
 *
 * Each not authorized usage is prohibited.
 */

/**
 * @author Federico Bernardin <federico.bernardin@pallino.it>
 * @created 15/02/16
 */

namespace Pallino\Translator\Utility;


class XliffUtility {

    protected static function getXlfTextForWriting(array $translations, $language, $defaultLanguage) {
        $result = '';
        foreach ($translations as $key => $match) {
            if ($key != 'languages') {
                if($language == $defaultLanguage){
                    $result .= sprintf("\n" . '<trans-unit id="%s">' . "\n\t" . '<source>%s</source>' . "\n" . '</trans-unit>', $key, $match[$language]);
                }
                else{
                    $result .= sprintf("\n" . '<trans-unit id="%s">' . "\n\t" . '<source>%s</source>' .  "\n\t" . '<target>%s</target>' . "\n" . '</trans-unit>', $key, $match[$defaultLanguage], $match[$language]);
                }
            }
        }
        return $result;
    }

    public static function writeToFileXliff($file, $translationData, $language, $defaultLanguage = 'en') {
        $translationBody = self::getXlfTextForWriting($translationData, $language, $defaultLanguage);
        $date = date("Y-m-d\TH:i:sP");
        $translationData = <<<EOF
<?xml version="1.0" encoding="utf-8"?>
<xliff version="1.0">
    <file source-language="$language" datatype="plaintext" original="messages" product-name=""
          date="$date">
        <header/>
        <body>$translationBody</body></file></xliff>
EOF;
        file_put_contents($file, $translationData);
    }
}